#!/usr/bin/env bash

if [[ $# -lt 3 ]]
then
    echo Usage: $0 proc qubits times [error]
    exit 0
fi

for (( i=0 ; i<$3 ; ++i ))
do
    ./runNoise_PQe.sh $1 $2 $4 
done

