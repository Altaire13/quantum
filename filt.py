#!/usr/bin/env python

import sys

def main():
    if len(sys.argv) < 2:
        print 'Usage:', sys.argv[0], 'file'
        sys.exit(0)

    file = open(sys.argv[1])

    lines = file.read().splitlines()

    # (qubits, procs)  ->  (tokens, fulltime, count)
    map = {}

    for line in lines:
        # (0)cmd (1)procs (2)qubits ... (7)fulltime (8)target
        spl = line.split()
        if len(line) == 0 or spl[0] == 'Cmd' or len(spl) < 8:
            continue

        #     (     qubits,       procs,      target)
        tup = (int(spl[2]), int(spl[1]), int(spl[8])) 

        cur_time = float(spl[7])

        if tup in map:
            prev_time = map[tup][1]

            if cur_time < prev_time:
                map[tup] = (spl[1:],     cur_time,  map[tup][2] + 1)
            else:
                map[tup] = (map[tup][0], prev_time, map[tup][2] + 1)
        else:    
            map[tup] =     (spl[1:],     cur_time,  1)

    for i in sorted(map):
        j = (i[0], 1, i[2])
        if (not j in map):
            j = (i[0], 2, i[2])
            base_time = map[j][1] * 2
        else:
            base_time = map[j][1]

        print ' '.join(map[i][0]), map[i][2], base_time / map[i][1]
        #print map[i]


if __name__ == '__main__':
    main()

