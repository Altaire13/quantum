#!/usr/bin/env python

import sys
import matplotlib.pyplot as plt
import subprocess as sp

def getData(filename):
    mdata = {}
    try:
        with open(filename, 'r') as file:
            lines = file.read().splitlines()
    
            for line in lines:
                # (0)qubits (1)error (2)len (3..) [1-F]
                spl = line.split()
        
                if len(spl) < 4:
                    continue
        
                tup = (int(spl[0]), float(spl[1]))
        
                mdata[tup] = [float(i) for i in spl[3:]]
    except EnvironmentError:
        print 'Error reading file'
    
    return mdata

def runJobs(foutname, qubits, count, error):
    #q = sp.Popen(['./runNoise_PQTe.sh', str(count), '-N', '-e', '0.001'], stdout=sp.PIPE)
    print 'run'
    q = sp.call(['./runNoise_PQTe.sh', '4', str(qubits), str(count), str(error)])
    print 'q', q
    #(c_out, c_err) = q.communicate()
#     print 'Output:', c_out
#     print 'Error:', c_err
#     print 'End'
    fid_data = open(foutname, 'w')
    q = sp.call(['./filt_fid.py', 'fid.log'], stdout=fid_data)
    #(c_out, c_err) = q.communicate()
    fid_data.close()
    #tasks = c_out.splitlines()[2:]

class Handle:
    def __init__(self, fig, fname, qubits, error, step):
        self.fig = fig
        self.fname = fname
        self.qubits = qubits
        self.error = error
        self.step = step
        
        data = getData(fname)
        if (self.qubits, self.error) not in data:
            runJobs(self.fname, self.qubits, self.step, self.error)
            data = getData(fname)
            
        if (self.qubits, self.error) in data:
            self.data = data[self.qubits, self.error]
        else:
            self.data = [0]
            
        self.bins = len(self.data) / 5 + 1 

        self.cid = self.fig.canvas.mpl_connect('key_press_event', self)

        self.draw()
        plt.show()
        
    def draw(self):
        self.fig.clear()
        plt.hist(self.data, self.bins)
        plt.draw()

    def __call__(self, event):
        if event.key == '.':
            self.bins += 1
            self.draw()
        elif event.key == ',':
            self.bins -= 1
            self.draw()
        elif event.key == 'l':
            self.bins *= 1.5
            self.draw()
        elif event.key == 'k':
            self.bins /= 1.5
            self.draw()
        elif event.key == 'u':
            runJobs(self.fname, self.qubits, self.step, self.error)
            self.data = getData(self.fname)[self.qubits, self.error]
            self.draw()





def main():
    if len(sys.argv) < 5:
        print 'Usage:', sys.argv[0], 'file', 'qubits', 'error', 'step'
        sys.exit(0)
    
    fname = sys.argv[1]
    qubits = int(sys.argv[2])
    error = float(sys.argv[3])
    step = int(sys.argv[4])

    fig = plt.figure()
    h = Handle(fig, fname, qubits, error, step)


if __name__ == '__main__':
    main()


