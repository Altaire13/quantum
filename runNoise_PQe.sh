#!/usr/bin/env bash

if [[ $# -lt 2 ]]
then
    echo Usage: $0 proc qubits [error]
    exit 0
fi

if [[ $# -lt 3 ]]
then
    mpirun -n $1 ./evol $2 -N
else
    mpirun -n $1 ./evol $2 -N -c -e $3
fi


