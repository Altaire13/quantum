#!/usr/bin/env python

import sys

def main():
    if len(sys.argv) < 2:
        print 'Usage:', sys.argv[0], 'file'
        sys.exit(0)

    file = open(sys.argv[1])

    lines = file.read().splitlines()

    map = {}

    for line in lines:
        # (0)qubit (1)error (2)1-F
        spl = line.split()

        if len(spl) == 0:
            continue

        tup = (int(spl[0]), float(spl[1]))

        if tup in map:
            map[tup] += [float(spl[2])]
        else:
            map[tup] = [float(spl[2])]

    for t in sorted(map):
        print '{0} {1}'.format(*t), len(map[t]), ' '.join(str(i) for i in map[t])


if __name__ == '__main__':
    main()

