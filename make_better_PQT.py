#!/usr/bin/env python

import subprocess as sp
import sys

if len(sys.argv) < 4:
    print 'Usage:', sys.argv[0], 'procs', 'qubits', 'tar'
    sys.exit(0)

tup = map(int, sys.argv[1:4])

prev = -1

filt = sp.Popen(['./filt.py', 'log.out'], stdout=sp.PIPE)
(c_out, c_err) = filt.communicate()

if filt.returncode:
    print 'No file?'
else:
    for line in c_out.splitlines():
        spl = line.split()
        if map(int, spl[:2] + [spl[7]]) == tup:
            prev = float(spl[6])
            break

if prev == -1:
    prev = 100000000

print 'Was    :', prev
print


cur = prev

launched = 0

while prev <= cur:
    run = sp.Popen(['./mpi_Q_PROC_tar.sh', sys.argv[2], sys.argv[1], sys.argv[3]], 
            stdout=sp.PIPE)
    (c_out, c_err) = run.communicate()
    launched += 1
    cur = float(c_out.split()[0])
    print 'Current:', cur, '\t', cur / prev, '\t', launched
    #break

print prev, '->', cur, '\t', prev / cur

print 'Launched:', launched

