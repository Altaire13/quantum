#!/usr/bin/env bash

if [[ $# -lt 2 ]]
then
    echo Usage: $0 qubits procs [target]
    exit 0
fi

if [[ $# -lt 3 ]]
then
    mpirun -n $2 ./evol $1 
else
    mpirun -n $2 ./evol $1 -q $3
fi

