/*
 * DistributedQubit.cpp
 *
 *  Created on: Apr 5, 2016
 *      Author: alexey
 */

#include "DistributedQubit.h"

#include <mpi.h>
#include <cmath>
#include <cstdio>
#include <cstdlib>

void DistributedQubit::any_random_state(size_t regions) {
    if (regions == 0 ||
            regions < world_size) {
        if (regions > 0) {
            fprintf(stderr, "Wrong regions number (%lu with %d procs).\n"
                    "Doing simple generation.\n", regions, world_size);
        }

        random_state();
    } else {
        stable_random_state(regions);
    }
}

// Returns magnitude
void DistributedQubit::random_state() {
    sum = 0;
    for (unsigned i = 0; i < local_size; ++i) {
        state[i] = complex<double>(
                rand() * 2.0 / RAND_MAX - 1.0,
                rand() * 2.0 / RAND_MAX - 1.0);
        double t = std::abs(state[i]);
        sum += t * t;
    }

    MPI_Allreduce(MPI_IN_PLACE, &sum, 1, MPI_DOUBLE, MPI_SUM, MPI_COMM_WORLD);

    sum = sqrt(sum);
}

void DistributedQubit::stable_random_state(size_t regions) {
    if (full_size < regions) {
        regions = full_size;
    }

    int seeds[regions];
    for (int i = 0; i < regions; ++i) {
        seeds[i] = rand();
    }

    int local_regions = regions / world_size;
    int region_size = full_size / regions;
    int init_region = world_rank * local_regions;

    sum = 0;

    for (int reg = 0; reg < local_regions; ++reg) {
        srand(seeds[init_region+reg]);
        int reg_start = reg * region_size;

        for (unsigned i = 0; i < region_size; ++i) {
            state[reg_start+i] = complex<double>(
                    rand() * 2.0 / RAND_MAX - 1.0,
                    rand() * 2.0 / RAND_MAX - 1.0);
            double t = std::norm(state[reg_start+i]);
            sum += t;
        }
    }

    MPI_Allreduce(MPI_IN_PLACE, &sum, 1, MPI_DOUBLE, MPI_SUM, MPI_COMM_WORLD);

    sum = sqrt(sum);
}

void DistributedQubit::normalize() {
    for (unsigned i = 0; i < local_size; ++i) {
        state[i] /= sum;
    }
}

void DistributedQubit::permute(Permutation& perm) {
    calc_halves(perm);
    exchange_and_sum(perm);

    StateArray t = state;
    state = res0;
    res0 = t;
}

void DistributedQubit::calc_halves(Permutation& perm) {
    unsigned bit = (1 << perm.qubit);

    unsigned local    = local_size - 1;
    unsigned nonlocal = ~local;

    unsigned local_bit    = local & bit;
    unsigned nonlocal_bit = nonlocal & bit;

    for (unsigned i = 0; i < local_size; ++i) {
        unsigned tar, tar0, tar1;
        unsigned src0, src1;

        tar = (unsigned)(world_rank * local_size) + i;

        tar0 = tar;
        src0 = tar;

        /*if (local_size <= bit) { // Send
            tar1 = tar ^ bit;
            src1 = tar;
        } else {
            tar1 = tar;
            src1 = tar ^ bit;
        }*/

        tar1 = tar ^ nonlocal_bit;
        src1 = tar ^ local_bit;


        unsigned tar0_bit = ((tar0 & bit) >> perm.qubit);
        unsigned tar1_bit = ((tar1 & bit) >> perm.qubit);
        unsigned src0_bit = ((src0 & bit) >> perm.qubit);
        unsigned src1_bit = ((src1 & bit) >> perm.qubit);

        res0[i] = perm.u[tar0_bit][src0_bit] * state[src0 & local];
        res1[i] = perm.u[tar1_bit][src1_bit] * state[src1 & local];
    }
}

void DistributedQubit::exchange_and_sum(Permutation& perm) {
    if ((1 << perm.qubit) >= local_size) {
         int pair = ((world_rank * local_size) ^ (1 << perm.qubit)) / local_size;

         MPI_Sendrecv_replace(res1, local_size, MPI_DOUBLE_COMPLEX,
                 pair, 0, pair, 0, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
    }

    for (unsigned i = 0; i < local_size; ++i) {
        res0[i] += res1[i];
    }
}

int DistributedQubit::output_state(const char* filename) {
    if (qubits > 16) {
        if (world_rank == 0) {
            printf("Error: can't output more than 16 qubits.\n");
        }

        return -1;
    }

    if (world_rank == 0) {
        complex<double>* result = static_cast<complex<double>*>(malloc(sizeof(*result) * full_size));

        MPI_Gather(
                state,   local_size, MPI_DOUBLE_COMPLEX,
                result, local_size, MPI_DOUBLE_COMPLEX,
                0, MPI_COMM_WORLD);

        FILE* outFile = fopen(filename, "w");

        if (outFile) {
            fwrite(result, sizeof(result[0]), full_size, outFile);
            fclose(outFile);
        } else {
            printf("Cannot open output file\n");
        }

        free(result);
    } else {
        MPI_Gather(
                res0,   local_size, MPI_DOUBLE_COMPLEX,
                NULL,   local_size, MPI_DOUBLE_COMPLEX,
                0, MPI_COMM_WORLD);
    }

    return 0;
}

void DistributedQubit::print_state() {
    if (qubits > 7) return;

    if (world_rank == 0) {
        complex<double>* result = static_cast<complex<double>*>(malloc(sizeof(*result) * full_size));

        MPI_Gather(
                state,   local_size, MPI_DOUBLE_COMPLEX,
                result, local_size, MPI_DOUBLE_COMPLEX,
                0, MPI_COMM_WORLD);

        for (size_t i = 0; i < full_size; ++i) {
            printf("(%lf %lf) ", result[i].real(), result[i].imag());
        }
        printf("\n");

        free(result);
    } else {
        MPI_Gather(
                res0,   local_size, MPI_DOUBLE_COMPLEX,
                NULL,   local_size, MPI_DOUBLE_COMPLEX,
                0, MPI_COMM_WORLD);
    }
}

complex<double> dot(DistributedQubit& a, DistributedQubit& b) {
    size_t size = a.local_size;
    complex<double> sum;
    for (size_t i = 0; i < size; ++i) {
        sum += conj(a.state[i]) * b.state[i];
    }

    MPI_Allreduce(MPI_IN_PLACE, &sum, 1, MPI_DOUBLE_COMPLEX, MPI_SUM, MPI_COMM_WORLD);

    return sum;
}


