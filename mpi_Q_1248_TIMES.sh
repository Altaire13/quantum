#!/usr/bin/env bash

if [[ $# -lt 2 ]]
then
    echo Usage: $0 qubits times
    exit 0
fi


for (( i=0 ; i<$2 ; ++i ))
do
    for p in 1 2 4 8
    do
        echo $i $p
        mpirun -n $p ./evol $1
    done
done

