/*
 * DistributedQubit.h
 *
 *  Created on: Apr 5, 2016
 *      Author: alexey
 */

#ifndef DISTRIBUTEDQUBIT_H_
#define DISTRIBUTEDQUBIT_H_

#include <cstdlib>
#include <cstdio>
#include <complex>
#include <mpi.h>

#include <stdexcept>

using std::complex;

typedef complex<double>* StateArray;

typedef struct Permutation {
    size_t qubit;

    complex<double> u[2][2];

    void print() {
        printf("(%lf %lf) (%lf %lf)\n", u[0][0].real(), u[0][0].imag(),
                u[0][1].real(), u[0][1].imag());
        printf("(%lf %lf) (%lf %lf)\n", u[1][0].real(), u[1][0].imag(),
                u[1][1].real(), u[1][1].imag());
    }
} Permutation;

class DistributedQubit {
public:
    DistributedQubit(int world_size, int world_rank, size_t qubits) :
            world_size(world_size), world_rank(world_rank),
            qubits(qubits), full_size(1 << qubits), local_size(full_size / world_size) {
        if(world_size > full_size) {
            if (world_rank == 0) {
                fprintf(stderr, "Too many processors.\n");
            }
            throw std::runtime_error("Too many processors.\n");
        }

        state = static_cast<StateArray>(malloc(sizeof(*state) * local_size));
        res0  = static_cast<StateArray>(malloc(sizeof(*res0)  * local_size));
        res1  = static_cast<StateArray>(malloc(sizeof(*res1)  * local_size));

        sum = 0;
    }

    ~DistributedQubit() {
        free(state);
        free(res0);
        free(res1);
    }

    void any_random_state(size_t regions);
    void random_state();
    void stable_random_state(size_t regions);

    void normalize();

    void permute(Permutation& perm);

    void calc_halves(Permutation& perm);
    void exchange_and_sum(Permutation& perm);

    int output_state(const char* filename);
    void print_state();

    const int world_size;
    const int world_rank;

    const size_t qubits;

    const size_t full_size;
    const size_t local_size;

    StateArray state;
    StateArray res0;
    StateArray res1;

    double sum;
};

complex<double> dot(DistributedQubit& a, DistributedQubit& b);





#endif /* DISTRIBUTEDQUBIT_H_ */
