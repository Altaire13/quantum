#!/usr/bin/env bash

if [[ $# -lt 2 ]]
then
    echo Usage: $0 qubits procs times
    exit 0
fi


for (( i=0 ; i<$3 ; ++i ))
do
    echo $i
    mpirun -n $2 ./evol $1
done

