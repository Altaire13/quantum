#include <mpi.h>
#include <stdio.h>
#include <cstdlib>
#include <sys/param.h> // powerof2

#include <unistd.h>

#include <cstring>

#include <vector>
#include <complex>

#include <string>


#include "DistributedQubit.h"

using std::vector;
using std::complex;

//O3     -- none
//ofast  ++ much
//inline -- none
//native -- bad
//-fprof -- bad
//arrays -+ ??CALC?!?!?

// Memory usage: 3 * 2 ^ (qubits + 3 + 1) bytes <= 2 ^ (q + 6)

enum class ExperimentType {
    None,
    OnePermutation,
    NoizedNHadamar
};

typedef struct Config {
    size_t qubits;
    const char* outFile;
    size_t stable;
    size_t target;
    ExperimentType type;
    double error;
    bool check;

    int world_size;
    int world_rank;
} Config;


void printHelp(int argc, char* argv[]) {
    printf("Usage: %s [OPTIONS...] qubits\n\n", argv[0]);
    printf("OPTIONS may be:\n");
    printf("\t%-16s%s", "-h",         "print this help and exit\n");
    printf("\t%-16s%s", "-q target",  "set target qubit\n");
    printf("\t%-16s%s", "-f file",    "output file\n");
    printf("\t%-16s%s", "-s regions", "stable generation\n");
    printf("\t%-16s%s", "-P",         "One Permutation Experiment:\n");
    printf("\t%-16s%s", "-N",         "Noised N-Hadamar Experiment:\n");

    printf("\t%-16s%s", "-e error",   "error level for noise. Default: 0.01\n");
    printf("\t%-16s%s", "-c",         "check -- calc fidelity\n");
}

int parseArgs(int argc, char* argv[],
        Config* config) {

    config->error = 0.01;

    int arg;
    while ((arg = getopt(argc, argv, "hPNf:q:s:e:c")) != -1) {
        switch(arg) {
        case 'h':
            printHelp(argc, argv);
            return 1;
        case 'f':
            config->outFile = optarg;
            break;
        case 'q':
            config->target = strtoul(optarg, NULL, 10);
            break;
        case 's':
            config->stable = strtoul(optarg, NULL, 10);
            break;
        case 'P':
            if (config->type != ExperimentType::None) {
                fprintf(stderr, "Experiment type: choose only one\n");
                return -1;
            }
            config->type = ExperimentType::OnePermutation;
            break;
        case 'N':
            if (config->type != ExperimentType::None) {
                fprintf(stderr, "Experiment type: choose only one\n");
                return -1;
            }
            config->type = ExperimentType::NoizedNHadamar;
            break;
        case 'e':
            config->error = strtod(optarg, NULL);
            break;
        case 'c':
            config->check = true;
            break;
        default:
            return -1;
        }
    }

    if (config->type == ExperimentType::None) {
        fprintf(stderr, "Experiment type not set\n");
        return -1;
    }

    if (optind == argc) {
        return -1;
    }

    config->qubits = strtoul(argv[optind], NULL, 10);

    if (config->qubits == 0) {
        return -1;
    }

    return 0;
}

class NoisedNHadamarBenchmark {
public:
    NoisedNHadamarBenchmark(double error) :
        noise_seed(rand()), error(error), last_fidelity(1), last_start(0), last_end(0) {
        hada.u[0][0] =  M_SQRT1_2;
        hada.u[0][1] =  M_SQRT1_2;
        hada.u[1][0] =  M_SQRT1_2;
        hada.u[1][1] = -M_SQRT1_2;
    }

    double normal_dis_gen();

    void renoise();

    void nHadamar(DistributedQubit& qubit);
    void noisedNHadamar(DistributedQubit& qubit);

    void perform(Config config);

    void output(Config config);

private:
    unsigned int noise_seed;

    double error;

    double last_fidelity;

    double last_start;
    double last_end;

    Permutation hada;
    Permutation noisedHada;
};

double NoisedNHadamarBenchmark::normal_dis_gen() {
    double s = 0;
    for (size_t i = 0; i < 12; ++i) {
        s += static_cast<double>(rand_r(&noise_seed)) / static_cast<double>(RAND_MAX);
    }
    return s - 6;
}

void NoisedNHadamarBenchmark::renoise() {
    double t = normal_dis_gen() * error;
    double s = sin(t);
    double c = cos(t);

    Permutation tt;
    tt.u[0][0] = c;
    tt.u[0][1] = s;
    tt.u[1][0] =-s;
    tt.u[1][1] = c;

    noisedHada.u[0][0] = hada.u[0][0] * tt.u[0][0] + hada.u[0][1] * tt.u[1][0];
    noisedHada.u[0][1] = hada.u[0][0] * tt.u[0][1] + hada.u[0][1] * tt.u[1][1];
    noisedHada.u[1][0] = hada.u[1][0] * tt.u[0][0] + hada.u[1][1] * tt.u[1][0];
    noisedHada.u[1][1] = hada.u[1][0] * tt.u[0][1] + hada.u[1][1] * tt.u[1][1];
}

void NoisedNHadamarBenchmark::nHadamar(DistributedQubit& qubit) {
    for (size_t i = 0; i < qubit.qubits; ++i) {
        hada.qubit = qubit.qubits - i - 1;
        qubit.permute(hada);
    }
}

void NoisedNHadamarBenchmark::noisedNHadamar(DistributedQubit& qubit) {
    for (size_t i = 0; i < qubit.qubits; ++i) {
        renoise();

        noisedHada.qubit = qubit.qubits - i - 1;

        qubit.permute(noisedHada);
    }
}

void NoisedNHadamarBenchmark::perform(Config config) {
    last_start = MPI_Wtime();

    DistributedQubit noised =
            DistributedQubit(config.world_size, config.world_rank, config.qubits);


    unsigned int state_seed = time(NULL) + getpid();
    srand(state_seed);
    noised.random_state();
    noised.normalize();

    noisedNHadamar(noised);

    last_end = MPI_Wtime();

    MPI_Barrier(MPI_COMM_WORLD);

    if (config.check) {
        DistributedQubit clean =
                DistributedQubit(config.world_size, config.world_rank, config.qubits);
        srand(state_seed);
        clean.random_state();
        clean.normalize();

        nHadamar(clean);

        complex<double> sum = dot(clean, noised);

        last_fidelity = abs(sum) * abs(sum);
    } else {
        last_fidelity = 1;
    }

    output(config);
}

void NoisedNHadamarBenchmark::output(Config config) {
    if (config.world_rank == 0) {
        printf("%lf", last_end - last_start);

        FILE* log = fopen("noiseTime.log", "a");
        fprintf(log, "%lu %d %lf\n", config.qubits, config.world_size, last_end - last_start);
        fclose(log);

        if (config.check) {
            printf("\t%lf\n", 1 - last_fidelity);
            FILE* fid = fopen("fid.log", "a");
            fprintf(fid, "%lu %lf %.8e\n", config.qubits, config.error, 1 - last_fidelity);
            fclose(fid);
        } else {
            printf("\n");
        }
    }
}

void onePermutation(const char* cmd, Config config) {
    srand(0);

    size_t qubits = config.qubits;

    Permutation perm;
    perm.qubit = config.target;

    perm.u[0][0] =  1 / sqrt(2);
    perm.u[0][1] =  1 / sqrt(2);
    perm.u[1][0] =  1 / sqrt(2);
    perm.u[1][1] = -1 / sqrt(2);


    if (perm.qubit >= qubits) {
        perm.qubit = qubits - 1;
    }

    // Correct qubit number for correct bit shifting
    perm.qubit = qubits - perm.qubit - 1;

    MPI_Barrier(MPI_COMM_WORLD);

    double cp_begin = MPI_Wtime();

    DistributedQubit disQubit = DistributedQubit(config.world_size, config.world_rank, qubits);
    StateArray input_state = disQubit.state;
    StateArray res0 = disQubit.res0;
    StateArray res1 = disQubit.res1;


    double cp_meminit = MPI_Wtime();

    disQubit.any_random_state(config.stable);
    

    if (config.outFile) {
        disQubit.output_state((std::string("input_") + config.outFile).c_str());
    }

    double cp_vectorInit = MPI_Wtime();

    disQubit.normalize();

    double cp_normalized = MPI_Wtime();

    disQubit.permute(perm);

    double cp_calc = MPI_Wtime();


    if (config.outFile) {
        disQubit.output_state((std::string("output_") + config.outFile).c_str());
    }

    if (config.world_rank == 0) {
        double seg_meminit = cp_meminit    - cp_begin;
        double seg_gen     = cp_vectorInit - cp_meminit;
        double seg_normal  = cp_normalized - cp_vectorInit;
        double seg_calc    = cp_calc       - cp_normalized;

        double seg_all     = cp_calc       - cp_begin;

        FILE* log;
        log = fopen("log.out", "a");

        printf("%lf\n", seg_all);
        fprintf(log, "%-10s %-5d %-3lu %-10lf %-10lf %-10lf %-10lf %-10lf %3lu\n",
            strrchr(cmd, '/'), config.world_size, qubits, seg_meminit, seg_gen, seg_normal,
            seg_calc, seg_all, qubits - perm.qubit - 1);

        fclose(log);
    }
}

int main(int argc, char** argv) {
    MPI_Init(&argc, &argv);

    Config config;
    memset(&config, 0, sizeof(config));

    MPI_Comm_size(MPI_COMM_WORLD, &config.world_size);
    MPI_Comm_rank(MPI_COMM_WORLD, &config.world_rank);

    if (!powerof2(config.world_size)) {
        if (config.world_rank == 0) {
            printf("Wrong processors count.\n");
        }
        MPI_Finalize();
        return 0;
    }

    int r;
    if ((r = parseArgs(argc, argv, &config))) {
        if (r != 1 && config.world_rank == 0) {
            printf("Error in arguments.\n");
            printHelp(argc, argv);
        }

        MPI_Finalize();
        return 0;
    }

    if (config.type == ExperimentType::OnePermutation) {
        onePermutation(argv[0], config);
    } else if (config.type == ExperimentType::NoizedNHadamar) {
        NoisedNHadamarBenchmark noisedBench = NoisedNHadamarBenchmark(config.error);
        noisedBench.perform(config);
    } else {
        fprintf(stderr, "Wrong experiment type\n");
    }

    MPI_Finalize();
    return 0;
}
