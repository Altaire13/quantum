#!/usr/bin/env bash

if [[ $# -lt 2 ]]
then
    echo Usage: $0 min max times
    exit 0
fi


for (( i=$1 ; i<=$2 ; ++i ))
do
    echo "----- Q" $i
    ./mpi_Q_1248_TIMES.sh $i $3
done



