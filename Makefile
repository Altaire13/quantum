.PHONY: all

all: evol evolg

evol: main.cpp DistributedQubit.cpp DistributedQubit.h 
	mpicxx -O3 -std=c++11 main.cpp DistributedQubit.cpp -o evol
	
evolg: main.cpp DistributedQubit.cpp DistributedQubit.h
	mpicxx -g -O0 -std=c++11 main.cpp DistributedQubit.cpp -o evolg

float: float.cpp
	g++ float.cpp -O3 -o float

clean:
	-rm evol
